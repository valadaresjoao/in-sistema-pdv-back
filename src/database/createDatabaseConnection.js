const { Sequelize } = require("sequelize")

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "./src/data/data.sqlite",
    define: {
        timestamps: true,
        underscored: true,
        underscoredAll: true,
      },
})

const verifyDataBaseConnection = async ()=>{
    try {
        await sequelize.authenticate()
    } catch (error) {
        console.log(error)
    }
}

verifyDataBaseConnection()

module.exports = sequelize
