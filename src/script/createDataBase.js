const sequelize = require("../database/createDatabaseConnection")

async function createDatabaseConnection(){
    try {
        await sequelize.sync()
        console.log("Database created")
    } catch (error) {
        console.log(error)    
    }
}


module.exports = createDatabaseConnection


