const express = require("express")
const app = express()
const cors = require("cors")
const PORT = 3000

const createDatabaseConnection = require("./script/createDataBase")

try {
    createDatabaseConnection()
} catch (error) {
    console.log(error)
}



